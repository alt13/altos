#!/usr/bin/env bash

# Install nix
# https://www.addictivetips.com/ubuntu-linux-tips/use-the-nix-package-manager-on-any-linux-os/
# https://christitus.com/nix-package-manager/
sh <(curl -L https://nixos.org/nix/install) --no-daemon
nix-channel --update
nix-env -qa


nix-env -iA nixpkgs.telegram-desktop
nix-env -iA nixpkgs.celluloid
