#!/usr/bin/env bash
######################################
#     ___   _    _____ __   _____    #
#    / _ \ | |  |_   _/  | |____ |   #
#   / /_\ \| |    | | `| |     / /   #
#   |  _  || |    | |  | |     \ \   #
#   | | | || |____| | _| |_.___/ /   #
#   \_| |_/\_____/\_/ \___/\____/    #
#      https://gitlab.com/alt13      #
######################################

blue='\e[34m%s\e[0;37m%s\e[34m%s\e[0;37m%s\e[34m%s\e[0m%s\n'
green='\n\e[32m%s\e[0m%s\n'


print_fedora_logo(){
    printf $blue '                                        ' '' '' '' '' '                                              '
    printf $blue '              .",;::::;,".              ' '' '' '' '' '                                              '
    printf $blue '          .";:cccccccccccc:;,.          ' '' '' '' '' '                                              '
    printf $blue '       .;cccccccccccccccccccccc;.       ' '' '' '' '' '                                              '
    printf $blue '     .:cccccccccccccccccccccccccc:.     ' '' '' '' '' '                                              '
    printf $blue '   .;ccccccccccccc;' '.:dddl:.' ';ccccccc;.   ' '' '' '                                              '
    printf $blue '  .:ccccccccccccc;' 'OWMKOOXMWd' ';ccccccc:.  ' '' '' '                                              '
    printf $blue ' .:ccccccccccccc;' 'KMMc' ';cc;' 'xMMc' ';ccccccc:. ' '       d8888 888 888     .d88888b.   .d8888b. '
    printf $blue ' ,cccccccccccccc;' 'MMM.' ';cc;' ';WW:' ';cccccccc, ' '      d88888 888 888    d88P" "Y88b d88P  Y88b'
    printf $blue ' :cccccccccccccc;' 'MMM.' ';cccccccccccccccc: ' '' '' '     d88P888 888 888    888     888 Y88b.     '
    printf $blue ' :ccccccc;' 'oxOOOo' ';' 'MMM0OOk.' ';cccccccccccc: ' '    d88P 888 888 888888 888     888  "Y888b.  '
    printf $blue ' cccccc;' '0MMKxdd:' ';' 'MMMkddc.' ';cccccccccccc; ' '   d88P  888 888 888    888     888     "Y88b.'
    printf $blue ' ccccc;' 'XM0;' ';cccc;' 'MMM.' ';cccccccccccccccc; ' '  d88P   888 888 888    888     888       "888'
    printf $blue ' ccccc;' 'MMo' ';ccccc;' 'MMW.' ';ccccccccccccccc;  ' ' d8888888888 888 Y88b.  Y88b. .d88P Y88b  d88P'
    printf $blue ' ccccc;' '0MNc.' 'ccc' '.xMMd' ';ccccccccccccccc;   ' 'd88P     888 888  "Y888  "Y88888P"   "Y8888P" '
    printf $blue ' cccccc;' 'dNMWXXXWM0:' ';cccccccccccccc:,    ' '' '' '                                              '
    printf $blue ' cccccccc;' '.:odl:.' ';cccccccccccccc:,.     ' '' '' '          F E D O R A   E D I T I O N         '
    printf $blue ' :cccccccccccccccccccccccccccc:".       ' '' '' '' '' '                                              '
    printf $blue ' .:cccccccccccccccccccccc:;,..          ' '' '' '' '' '                gitlab.com/alt13              '
    printf $blue '   :::cccccccccccccc::;,.               ' '' '' '' '' '                                              '
}
print_fedora_logo


ask_usr(){
    printf $green "Have you install configuration? y/n"
    while true; do
        read answer
        if [ "$answer" = "y" ]; then
            break
        elif [ "$answer" = "n" ]; then
            printf $green "Do you want to proceed? y/n"
            while true; do
                read answer
                if [ "$answer" = "y" ]; then
                    break
                else
                    exit 1
                fi
            done
            break
        fi
    done
}
ask_usr


if command -v ansible; then
    :
else
    printf $green "[Updating system, please wait...]"
    sudo dnf -y update
    printf $green "[Installing ansible, please wait...]"
    sudo dnf -y install ansible 
fi


printf $green "[Starting ansbile playbook]"
sudo ansible-playbook fedora_pb.yml


ask_for_reboot(){
    printf $green "Do you want to reboot (required after first run)? y/n"
    while true; do
        read answer
        if [ "$answer" = "y" ]; then
            printf $green "Rebooting..."
            sleep 3 && reboot
            break
        elif [ "$answer" = "n" ]; then
            break
        fi
    done
}
ask_for_reboot
