#!/bin/bash

i="flatpak install"

$i "easyeffects"
# $i "com.brave.Browser"
$i "org.keepassxc.KeePassXC"
$i "md.obsidian.Obsidian"
$i "com.github.tchx84.Flatseal"
# $i "org.flameshot.Flameshot" # works finicky on wayland
$i "org.gimp.GIMP"
$i "org.gimp.GIMP.Plugin.Resynthesizer"
$i "org.mozilla.Thunderbird"
# $i "com.getmailspring.Mailspring"
# $i "com.obsproject.Studio"
# $i "io.mpv.Mpv"
# $i "io.github.celluloid_player.Celluloid"  # aur
# $i "org.videolan.VLC"
# $i "org.mozilla.firefox"
$i "com.dropbox.Client"
$i "org.signal.Signal"
$i "org.telegram.desktop"
# $i "com.viber.Viber"
$i "org.kryogenix.Pick"
# $i "io.github.peazip.PeaZip"
$i "org.qbittorrent.qBittorrent"
# $i "io.github.Qalculate"
$i "org.gnome.Calculator"
$i "org.gnome.FontManager"
# $i "com.github.jeromerobert.pdfarranger"
# $i "io.lmms.LMMS"
# $i "com.usebottles.bottles"
$i "org.gnome.baobab"
$i "org.gnome.Boxes"
# $i "com.github.hluk.copyq"  # blacklist: https://copyq.readthedocs.io/en/latest/faq.html
# $i "com.gitlab.newsflash"  # RSS
# $i "com.valvesoftware.Steam"
# $i "net.davidotek.pupgui2"  # ProtonUp-Qt (dependencies 'yad', 'xdotool')
# $i "com.heroicgameslauncher.hgl"
$i "org.nickvision.tubeconverter"  # basic yt-dlp frontend (command-line program that lets you easily download videos and audio)
$i "org.libreoffice.LibreOffice"


