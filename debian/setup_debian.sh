#!/usr/bin/env bash

# Qtile dependencies
$i alsa-utils

# Terminal soft alternatives
$i "zoxide"                       # jump
$i "lsd"                          # 'ls' The next gen file listing command. Backwards compatible with ls
$i "bat"                          # 'cat' clone with syntax highlighting
# $i "bat-extras"                   # ^ grep, man, watch, diff
$i "duf"                          # 'df'
$i "fd"                           # 'find' fast and user-friendly alternative
$i "tldr"                         # 'man' simplify man pages with practical examples


$i "flatpak"
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo



$i "pipewire pipewire-{alsa,jack,pulse}"



# Function keys
$i "light"                        # control screen and keyboard brightness
$i "playerctl"                    # media contol for funcion keys



policykit-1-gnome
network-manager-gnome # contain nm-applet
thunar




#nvidia
apt install linux-headers-amd64

#suource
deb http://deb.debian.org/debian/ sid main contrib non-free non-free-firmware

sudo apt install nvidia-driver 
