su -
apt update
apt uppgrade

apt install sudo
usermod -aG sudo alt
# re-login


# Qtile 
sudo apt install pipx
pipx install qtile
pipx inject qtile requests wlroots          # wayland
pipx inject qtile requests pywlroots        # wayland
pipx inject qtile requests pywayland        # wayland
pipx inject qtile requests xkbcommon # wayland
pipx inject qtile requests psutil    # widgets dependency
# from docs
sudo apt install xserver-xorg xinit
sudo apt install libpangocairo-1.0-0
sudo apt install python3-pip python3-xcffib python3-cairocffi
# echo 'qtile start' > ~/.xinitrc

# System
# https://github.com/Raymo111/i3lock-color
sudo apt install autoconf gcc make pkg-config libpam0g-dev libcairo2-dev libfontconfig1-dev libxcb-composite0-dev libev-dev libx11-xcb-dev libxcb-xkb-dev libxcb-xinerama0-dev libxcb-randr0-dev libxcb-image0-dev libxcb-util0-dev libxcb-xrm-dev libxkbcommon-dev libxkbcommon-x11-dev libjpeg-dev
git clone https://github.com/Raymo111/i3lock-color.git
cd i3lock-color
./install-i3lock-color.sh

sudo apt install zsh
sudo apt install zoxide
sudo apt install lsd
chsh
/bin/zsh

# setup configs
sudo apt install git
sudo apt install thunar

# Drivers
sudo apt install pipewire pipewire-{alsa,jack,pulse}
sudo apt install light
# Touchpad
# /etc/X11/xorg.conf.d/30-touchpad.conf
Section "InputClass"
    Identifier "touchpad"
    Driver "libinput"
    MatchIsTouchpad "on"
    Option "Tapping" "on"
    Option "TappingButtonMap" "lrm"
    Option "NaturalScrolling" "true"
EndSection

sudo apt install cryptsetup
sudo apt install gparted

sudo apt install lxappearance
sudo apt install nitrogen
sudo apt install picom
sudo apt install dunst

sudo apt install neovim
sudo apt install neofetch
sudo apt install rofi

sudo apt install kitty
sudo apt install curl

# nvim
# https://github.com/neovim/neovim-releases/releases
curl -LO https://github.com/neovim/neovim-releases/releases/download/nightly/nvim.appimage
chmod u+x nvim.appimage
sudo mv nvim.appimage /usr/local/bin
sudo mv nvim.appimage nvim

sudo apt install flatpak
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
$i "com.brave.Browser"
