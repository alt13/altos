# https://bbs.archlinux.org/viewtopic.php?id=297785

External displays not waking up after suspend (Hyprland, NVIDIA)



$ sudo vim /etc/modprobe.d/nvidia.conf
  # add: options nvidia NVreg_PreserveVideoMemoryAllocations=1
$ sudo mkinitcpio -P
$ sudo systemctl enable nvidia-suspend.service nvidia-hibernate.service
$ reboot
