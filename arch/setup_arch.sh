#!/usr/bin/env bash
######################################
#     ___   _    _____ __   _____    #
#    / _ \ | |  |_   _/  | |____ |   #
#   / /_\ \| |    | | `| |     / /   #
#   |  _  || |    | |  | |     \ \   #
#   | | | || |____| | _| |_.___/ /   #
#   \_| |_/\_____/\_/ \___/\____/    #
#      https://gitlab.com/alt13      #
######################################

blue='\n\e[34m%s\e[0m%s\n'
blue_bg='\n\e[7;34m%s\e[0m%s\n'
green='\n\e[32m%s\e[0m%s\n'
print_arch_logo(){
    printf $blue '                  -`               '
    printf $blue '                 .o+`              '
    printf $blue '                `ooo/              '
    printf $blue '               `+oooo:             '
    printf $blue '              `+oooooo:            '     '       d8888 888 888     .d88888b.   .d8888b. '
    printf $blue '              -+oooooo+:           '     '      d88888 888 888    d88P" "Y88b d88P  Y88b'
    printf $blue '            `/:-:++oooo+:          '     '     d88P888 888 888    888     888 Y88b.     '
    printf $blue '           `/++++/+++++++:         '     '    d88P 888 888 888888 888     888  "Y888b.  '
    printf $blue '          `/++++++++++++++:        '     '   d88P  888 888 888    888     888     "Y88b.'
    printf $blue '         `/+++ooooooooooooo/`      '     '  d88P   888 888 888    888     888       "888'
    printf $blue '        ./ooosssso++osssssso+`     '     ' d88P    888 888 Y88b.  Y88b. .d88P Y88b  d88P'
    printf $blue '       .oossssso-````/ossssss+`    '     'd88P     888 888  "Y888  "Y88888P"   "Y8888P" '
    printf $blue '      -osssssso.      :ssssssso.      '
    printf $blue '     :osssssss/        osssso+++.     ' '         A R C H    E D I T I O N'
    printf $blue '    /ossssssss/        +ssssooo/-     '
    printf $blue '  `/ossssso+/:-        -:/+osssso+-   ' '             gitlab.com/alt13    '
    printf $blue ' `+sso+:-`                 `.-/+oso:  '
    printf $blue '`++:.                           `-/+/ '
    printf $blue '.`                                 `/ '
    printf $blue '                                      '
}
print_arch_logo

ask_usr(){
    printf $green "Make sure that AUR helper and configs are installed. Do you want to proceed? y/n"
    while true; do
        read answer
        if [ "$answer" = "y" ]; then
            break
        else
            exit 1
        fi
    done
}
ask_usr





#
# VARIABLES
#

i='paru -S --noconfirm'





#
# DISPLAY SERVER
#

# xorg is group of packages that includes xorg-server package
sudo pacman -S xorg 

# The xinit program allows a user to manually start an Xorg display server, typically used to start window managers or desktop environments
sudo pacman -S xorg-xinit

# Create the X initiation file. It has the name .xinitrc and placed in your home directory (kde example) 'dbus-launch' - in order for Flameshot to start when using its sub-commands (flameshot gui)
# No display mangaer required
# echo "exec dbus-launch qtile start" > ~/.xinitrc
# for kde - "exec startkde"





#
# DISPLAY (login) MANAGER
#
# $i "ly"
# sudo systemctl enable ly.service





#
# PACKAGE MANAGERS
#

$i "flatpak"
# $i "python-pipx"  # install and run python applications in isolated environments




#
# WINDOW MANAGER
#

# Qtile
# $i "qtile"
# $i "alsa-utils"                    # qtile volume widget require "amixer"
# $i "python-pywlroots"              # to run Qtile as a Wayland compositor 




#
# DRIVERS
#

# Power management
$i "auto-cpufreq"
systemctl start auto-cpufreq
systemctl enable auto-cpufreq

# Audio
# installed with gnome
$i "pipewire-alsa"
$i "pipewire-jack"
$i "pipewire-pulse"
$i "pavucontrol"                  # GTK PulseAudio Volume Control

# Bluetooth
$i "bluez bluez-utils"            # 'bluez-firmware' - for Broadcom BCM203x and STLC2300 Bluetooth chips
$i "blueberry"                    # bluetooth manager (have option to hide tray icon)
sudo usermod -aG rfkill $USER

# Function keys
$i "light"                        # control screen and keyboard brightness
$i "playerctl"                    # media contol for funcion keys

# Nvidia
$i "nvidia-lts nvidia-utils nvidia-settings"  # ('nvidia-lts' for lts kernel)
$i "nvidia-prime"                   # use 'prime-run programName' to launch with nvidia GPU 
$i "nvtop"                          # GPUs process monitoring for AMD, Intel and NVIDIA

# Android
$i "mtpfs"                          # MTP support
$i "gvfs-mtp"                       # integrating the MTP protocol with your File Manager such as Nautilus
$i "gvfs-gphoto2"                   # PTP support for your File Manager
$i "scrcpy"                         # mirror android screen





#
# STYLES
#

$i "picom"                        # Graphical effects (transparency, drop shadowing)
$i "nitrogen"                     # Set wallpapers
# $i "lxappearance"               # set GTK theme X11
$i "nwg-look"                     # set GTK theme Wayland
$i "kvantum"                      # set QT theme
echo "QT_STYLE_OVERRIDE=kvantum" | sudo tee -a /etc/environment





#
# SYSTEM
#

$i "grim"   # take screenshot
$i "slurp"  # select area for screenshot
$i "swappy" # edit screenshot

$i "newsflash"

$i "gnome-system-monitor"
$i "gnome-encfs-manager-bin"      # encrypt folder
# $i "rofi"                         # run prompt
$i "tofi"                         # run prompt
#$i "copyq"                       # run prompt
# $aur "rofi-greenclip"                   # clipboard manager
$i "thunar"
$i "file-roller"                  # gnome archive manager (issues with .rar with password)
$i "network-manager-applet"       # tray nm-applet
$i "polkit-gnome"                 # allows unprivileged processes to speak to privileged processes # add to startup
$i "arandr"                       # (set resolution) - gui version of xrandr
$i "dunst"                        # notification deamon # notify-send NOTIFICATION TEST 	# test
$i "i3lock-color"                     # i3 screen locker
# $i "swaylock"                     # i3 screen locker for wayland
$i "redshift" 	                # adjusts the computer display's color temperature based upon the time of day
    # redshift-gtk -l 52.37:4.9         # set geo location to avoid error
#$i "caffeine-ng"
#$i "barrier"

# Firewall
#$aur "portmaster-stub-bin"





#
# TERMINAL
#

# Terminal emulator
$i "zsh zsh-syntax-highlighting zsh-autosuggestions"
#echo "Type: /bin/zsh"
#chsh $USER
$i "kitty"                        # alacritty nvim "/home" - doesn't work
$i "neovim"
# $i "neovide"
$i "xclip"                        # vim is not compiled with the clipboard feature

# Terminal soft alternatives
$i "zoxide"                       # jump
$i "lsd"                          # 'ls' The next gen file listing command. Backwards compatible with ls
$i "bat"                          # 'cat' clone with syntax highlighting
$i "bat-extras"                   # ^ grep, man, watch, diff
$i "duf"                          # 'df'
$i "fd"                           # 'find' fast and user-friendly alternative
$i "tldr"                         # 'man' simplify man pages with practical examples

# Terminal additional soft
#$i "tree"
$i "fzf"                          # fuzzy finder
$i "ripgrep"                      # recursively search
$i "fastfetch"
$i "btop"
$i "powertop"
$i "tmux"
$i "ncdu"
$i "wget curl"
$i "rsync"
$i "reflector"                    # update mirrors
$i "ntfs-3g"                      # support for NTFS
$i "openssh"                      # working with ssh
# $i "git-delta-git"                # viewer for git and diff output





#
# SOFT
#

# Browser
# $i ""
# Productivity
$i "eog"                          # Eye of GNOME imgae viewer
$i "gpaste"                       # gnome clipboard manager
# Monitoring
$i "gparted"

# Gaming
# $i "steam"


# Server stuff
#$i "docker"
#$i "docker-compose"


$i "celluloid"
$i "floorp-bin"



#
# DEPENDENCIES
#
# pip install subliminal                  # mpv - davidde/mpv-autosub
# $i "gnome-keyring"                      # for mailspring





printf $green "It's recommended to reboot the system"
