#!/bin/bash


# Make flatpak follow user styles (icons and fonts)
# "/home/{{ username }}/.local/share/flatpak/overrides/global"
# [Context]
# filesystems=/home/{{ username }}/.themes:ro;/home/{{ username }}/.fonts:ro;/home/{{ username }}/.icons:ro;xdg-config/Kvantum:ro;

# https://itsfoss.com/flatpak-app-apply-theme/
sudo flatpak override --env=GTK_THEME=Material-Black-Blueberry_2.9.9
sudo flatpak override --env=QT_STYLE_OVERRIDE=kvantum
# sudo flatpak override --env=ICON_THEME=my-icon-theme

# Allow flatpaks to access styles
# rm /home/alt/.local/share/icons
# ln -s /home/alt/.icons /home/alt/.local/share/icons

# rm /home/alt/.local/share/fonts
# ln -s /home/alt/.fonts /home/alt/.local/share/fonts

# rm /home/alt/.local/share/themes
# ln -s /home/alt/.themes /home/alt/.local/share/themes



# Flatpak locations
cd ~/.local/share/flatpa /usr/share/icons/default/index.themeoverrides
vim /usr/share/icons/default/index.theme
vim /var/lib/flatpak/overrides/global
flatpak info -M com.github.tchx84.Flatseal


https://www.linuxfordevices.com/tutorials/linux/themeing-flatpak-application
